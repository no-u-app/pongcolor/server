using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Server
{
    public class Server
    {
        public Server()
        {
            TcpListener server = null;

            try
            {
                int port = 13000;
                IPAddress localAddr = IPAddress.Parse("127.0.0.1");

                server = new TcpListener(localAddr, port);

                server.Start();

                byte[] bytes = new byte[256];

                while (true)
                {
                    Console.WriteLine("Listening on port " + port);

                    TcpClient client = server.AcceptTcpClient();
                    Console.WriteLine("Connected!");

                    Send(client, Receive(client));

                    client.Close();
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            finally
            {
                server.Stop();
            }

            Console.WriteLine("Server shut down.");
        }

        public void Send(TcpClient client, string data)
        {
            byte[] msg = System.Text.Encoding.ASCII.GetBytes(data);

            client.GetStream().Write(msg, 0, msg.Length);
            Console.WriteLine("Sent: {0}", data);
        }

        public string Receive(TcpClient client)
        {
            NetworkStream stream = client.GetStream();

            byte[] bytes = new byte[256];
            string data = "";

            int i;
            while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
            {
                data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                Console.WriteLine("Received: {0}", data);

                break;
            }

            return data;
        }
    }
}
